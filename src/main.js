import Vue from "vue";
import App from "./app/App.vue";
import "./app/index.css";

import {BootstrapVue, IconsPlugin} from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";

import { store } from '@/app/store';

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);



Vue.config.productionTip = false;

new Vue({
	render: (h) => h(App),
	store,
}).$mount("#app");

export const parseDate = (date) => {
	try {
		return new Intl.DateTimeFormat({
			dateStyle: "short",
			timeStyle: "full",
			hour12: true,
		}).format(new Date(date));
	} catch {
		return "";
	}
};

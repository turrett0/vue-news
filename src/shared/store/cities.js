import {citiesAPI} from "../http/cities";

export const citiesModule = {
	state: {
		cities: {},
		isLoading: false,
		selectedCity: null,
	},
	mutations: {
		changeIsLoading(state, isLoading) {
			state.isLoading = isLoading;
		},
		changeSelectedCity(state, selectedCity) {
			state.selectedCity = selectedCity;
		},
		setCities(state, cities) {
			state.cities = cities;
		},
	},
	actions: {
		async getCities({commit}) {
			try {
				const cities = await citiesAPI.getCities();
				this.commit("setCities", cities);
			} catch (error) {
				console.log(error);
			} finally {
				commit("changeIsLoading", false);
			}
		},
	},
};

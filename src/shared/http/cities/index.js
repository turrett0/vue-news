import { http } from '..';

export const citiesAPI = {
	getCities: () => http.get("/cities").then((res) => res.data.data),
};

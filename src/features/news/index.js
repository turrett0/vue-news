import NewsCardWithModal from "./NewsCardWithModal.vue";
import NewsAddNewFormModal from "./NewsAddNewFormModal.vue";

export {NewsCardWithModal, NewsAddNewFormModal};

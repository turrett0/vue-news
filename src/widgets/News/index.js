import NewsWidget from "./components/NewsWidget.vue";
import NewsFiltersWidget from "./components/NewsFiltersWidget.vue";

export {NewsWidget, NewsFiltersWidget};

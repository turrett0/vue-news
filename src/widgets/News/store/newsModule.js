import {newsAPI} from "../http";

export const newsModule = {
	state: {
		news: [],
		isLoading: false,
		isMaxNews: false,
		cities: {},
		filters:{
			city:null,
			isFavorite:false
		}
	},
	mutations: {
		setNews(state, news) {
			state.news.push(...news);
		},
		setOneNews(state, news) {
			state.news.unshift(news);
		},
		setIsFavorite(state, newsId) {
			state.news = state.news.map((news) =>
				news.id === newsId ? {...news, isFavorite: !news.isFavorite} : news
			);
		},
		setIsLoading(state, payload) {
			state.isLoading = payload;
		},
		setIsMaxNews(state, payload) {
			state.isMaxNews = payload;
		},
		clearNews(state) {
			state.news = [];
		},
		changeFilters(state, payload) {
			state.filters = {...state.filters,...payload};
		}

	},
	computed: {
		favoriteNews() {
			return this.news.filter((news) => news.isFavorite);
		},
	},
	actions: {
		async fetchNews({commit}, params) {
			try {
				commit("setIsLoading", true);
				const news = await newsAPI.getNews(params);
				commit("setNews", news);

				!news.length && commit("setIsMaxNews", true);
			} catch (error) {
				console.log(error);
			} finally {
				commit("setIsLoading", false);
			}
		},
		async createNews(_, payload) {
			try {
				await newsAPI.createNews(payload).then((data) => {
					if (data.status === 200) {
						alert("Success");
					}
				});
			} catch (error) {
				console.log(error);
			}
		},
	},
};

import {http} from "@/shared/http";

export const newsAPI = {
	getNews: (params) => http.get("/news", {params: params}).then((res) => res.data.data),
	createNews: (data) => http.post("/news", data),
};

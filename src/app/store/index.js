import {citiesModule} from "@/shared/store/cities";
import {newsModule} from "@/widgets/News/store/newsModule";
import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export const store = new Vuex.Store({
	modules: {
		news: newsModule,
		cities: citiesModule,
	},
});
